<?php

return [


    /*
     * The key of a taxonomy array will be used everywhere in the
     * code to identify this taxonomy. For simplicity, we've created
     * a default taxonomy called "Category".
     *
     * To register more taxonomies, just add them here.
     */

    'category' => [



        /*
         * Name of the Taxonomy.
         *
         * TODO: Make name translatable.
         */

        'name' => 'Category',





        /*
         * The slug to display in url. If omitted, the key will be used.
         * Use only alpha-numeric and dash. For performance reasons, we
         * recommend to give a static url-friendly string here.
         *
         * TODO: Make name translatable.
         */

        'slug' => 'category',

    ]



];
