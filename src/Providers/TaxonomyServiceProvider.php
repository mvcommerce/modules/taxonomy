<?php

namespace MVCommerce\Taxonomy\Providers;


use Illuminate\Support\ServiceProvider;
use MVCommerce\Taxonomy\Controllers\TermController;
use MVCommerce\Taxonomy\Taxonomy;

class TaxonomyServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');

        $this->publishes([
            __DIR__.'/../../migrations' => database_path('migrations'),
        ], ['mvcommerce', 'mvcommerce_taxonomy', 'mvcommerce_taxonomy_migrations', 'migrations']);


        $this->publishes([
            __DIR__.'/../../config/taxonomies.php' => config_path('mvcommerce/taxonomies.php'),
        ], ['mvcommerce', 'mvcommerce_taxonomy', 'mvcommerce_taxonomy_config', 'config']);


    }



    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {

        $this->mergeConfigFrom(
            __DIR__.'/../../config/taxonomies.php', 'mvcommerce.taxonomies'
        );

        $this->app->make(TermController::class);


        foreach ( config('mvcommerce.taxonomies') as $taxonomy => $definition ){
            Taxonomy::add($taxonomy, $definition);
        }

    }


}
