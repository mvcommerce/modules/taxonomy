<?php

namespace MVCommerce\Taxonomy;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class Taxonomy
{

    /** @var Taxonomy[]|array */
    public static $taxonomies;

    public $taxonomy, $name, $slug;

    public function __construct($key, $definition = [])
    {
        $this->taxonomy = $key;
        $this->name = $definition['name'] ?? $key;
        $this->slug = $definition['slug'] ?? $key;

        // TODO: Do the rest of the definition management here.
    }



    /**
     * Start querying terms related to the taxonomy.
     * @return Builder
     */
    public function terms(){
        return Term::taxonomy( $this->taxonomy );
    }


    /**
     * Get all registered taxonomies.
     *
     * @return Taxonomy[]|array
     */
    public static function all(){

        if(!static::$taxonomies){
            static::$taxonomies = [];
        }

        return static::$taxonomies;
    }


    /**
     * Get a specific taxonomy. This method is useful
     * when you don't know if the input is a string or
     * a Taxonomy object. This method always returns
     * the Taxonomy Object if found, null otherwise.
     *
     * @param string|Taxonomy $taxonomy
     * @return Taxonomy|null
     */
    public static function get($taxonomy){

        if( is_a($taxonomy, static::class) ){
            return $taxonomy;
        }

        if(isset(static::$taxonomies[$taxonomy])){
            return static::$taxonomies[$taxonomy];
        }

        return NULL;
    }


    /**
     *
     * Registers a new Taxonomy.
     *
     * @param string $taxonomy
     * @param array|\ArrayAccess $definition
     */
    public static function add(string $taxonomy, $definition = []){

        if(!static::$taxonomies){
            static::$taxonomies = [];
        }

        $taxonomyObj = new static($taxonomy, $definition);
        static::$taxonomies[$taxonomy] = $taxonomyObj;
    }


    /**
     * Removes a taxonomy from the registered taxonomies.
     *
     * @param $taxonomy
     */
    public static function remove($taxonomy){

        $taxonomy = static::get($taxonomy);
        if(!$taxonomy) return;

        unset(static::$taxonomies[$taxonomy->taxonomy]);
    }
}
