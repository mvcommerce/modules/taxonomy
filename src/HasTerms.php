<?php

namespace MVCommerce\Taxonomy;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphToMany;


/**
 * Trait HasTerms
 * @package MVCommerce\Taxonomy
 *
 * @property Term[]|Collection $terms
 */
trait HasTerms
{

    /**
     * @return MorphToMany
     */
    public function terms(){
        return $this->morphToMany( Term::class, 'object', 'object_term' );
    }

}
