<?php

namespace MVCommerce\Taxonomy;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

/**
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property int $id
 * @property int $parent_id
 * @property string $taxonomy
 * @property string $name
 * @property string $slug
 * @property string $description
 */
class Term extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id', 'taxonomy', 'name', 'slug', 'description'
    ];


    /**
     * @param string|object $type Class of that object.
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function objects($type){

        // We can't directly use any static class here.

        if( is_object($type) ){
            $related_class = get_class($type);
        }else if (is_string($type)){
            $related_class = $type;
        }else{
            throw new RelationNotFoundException();
        }

        return $this->morphedByMany($related_class, 'object', 'object_term');
    }


    /**
     * @param Builder $query
     * @param string|array|Taxonomy|Taxonomy[] $taxonomy
     */
    public function scopeTaxonomy(Builder $query, $taxonomy){
        $taxonomies = Arr::wrap($taxonomy);
        $taxonomies = array_map(function($taxonomy){
            return is_a($taxonomy, Taxonomy::class) ? $taxonomy->taxonomy : $taxonomy;
        }, $taxonomies);

        $query->whereIn('taxonomy', $taxonomies);

    }



    /**
     * @param Builder $query
     * @param string $slug
     */
    public function scopeSlug(Builder $query, $slug){
        $query->where('slug', $slug);

    }


    /**
     * Get the taxonomy object of this term
     * @return Taxonomy|null
     */
    public function taxonomy(){
        return Taxonomy::get($this->taxonomy);
    }

}
