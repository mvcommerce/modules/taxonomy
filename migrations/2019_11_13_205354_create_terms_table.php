<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('parent_id')->nullable()->index();
            $table->string('taxonomy');
            $table->string('name');
            $table->string('slug');
            $table->longText('description')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->unique(['taxonomy', 'slug']);


            $table->foreign('parent_id')
                ->references('id')->on('terms')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms');
    }
}
